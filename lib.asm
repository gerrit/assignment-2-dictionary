section .text

global exit
global string_length
global print_string
global print_strerr
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define IO_STDOUT 1
%define IO_STDERR 2
%define SYS_WRITE 1

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; character counter

    .countloop:
    cmp byte [rdi+rax], 0   ; testing for 0
    je .over
    inc rax                 ; increase the character counter
    jmp .countloop

    .over: ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length      ; let's use this to get the byte value
    pop rsi

    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, IO_STDOUT
    syscall
    ret

print_strerr:
    push rdi
    call string_length      ; let's use this to get the byte value
    pop rsi

    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, IO_STDERR
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi             ; we need to get a pointer from somewhere

    mov rdx, 1
    mov rax, IO_STDOUT
    mov rsi, rsp         ; so let's use a stack pointer
    mov rdi, SYS_WRITE

    syscall
    pop rdi              ; reset the stack
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jnl print_uint

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, 10
    sub rsp, 21           ; allocating the minimum we need because we're just that cool
    lea rcx, [rsp + 20]
    mov byte[rcx], 0

    .divloop:
    xor rdx, rdx
    div rsi; | |            dividing, turning the digit into ASCII
    add rdx, '0'
    dec rcx
    mov byte [rcx], dl    ; and appending it to the back of the string buffer
    test rax, rax
    jne .divloop

    mov rdi, rcx
    call print_string
    add rsp, 21           ; then deallocating the space we used
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax ; shift
    xor rdx, rdx ; temp storage

    .cmploop:
    mov rdx, [rsi+rax]
    cmp byte[rdi+rax], dl  ; if they are not equal could also mean that one of them ended
    jne .no
    test dl, dl   ; however, we're still checking for a 0 in case both of them did
    je .yes                ; then it's a yes
    inc rax
    jmp .cmploop

    .yes:
    mov rax, 1
    ret

    .no:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall

    cmp rax, 0          ; if nothing was there to read, nothing is there to do
    je .nope
    mov rax, [rsp]      ; else, return the read value
    .nope:
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

; (rdi, rsi) -> (r12, r14)
; r9 - current length
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r14, rsi
    xor r13, r13        ; character counter
                        ; loop back while al is a whitespace
    .whiteloop:
    call read_char
    cmp al, ' '
    je .whiteloop
    cmp al, 9           ; when I use \t here it stops working, a number seems to work fine
    je .whiteloop
    cmp al, 10          ; '\t' and '\n' are bigger than one byte, but their codes aren't
    je .whiteloop
    cmp rax, 0
    je .empty
                        ; didn't loop back, so it begins
                        ; stop looping when it's a whitespace or a terminator
    .charloop:
    cmp r13, r14
    jnb .overflow
    cmp al, ' '
    je .done
    cmp al, 9           ; same thing basically
    je .done
    cmp al, 10
    je .done
    cmp rax, 0
    je .done
                        ; that's a symbol, let's save it into the buffer

    mov byte [r12 + r13], al
    inc r13
    call read_char      ; reading the next something
    jmp .charloop

    .done:
    mov byte [r12 + r13], 0
    mov rax, r12
    mov rdx, r13
    pop r14
    pop r13
    pop r12
    ret

    .overflow:
    xor rax, rax
    xor rdx, rdx
    pop r14
    pop r13
    pop r12
    ret

    .empty:
    xor rax, rax
    mov rdx, 1
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:

    ; test.py somehow doesn't detect the SegFault in print_uint or print_int
    ; but it sure as ++++ detects it here when it runs them after parse_uint
    ; i don't even understand how it's possible to screw that up

    xor rsi, rsi
    xor rdx, rdx
    xor rax, rax ; current result
    xor r11, r11 ; digit counter
    mov r10, 10 ; for base 10 conversion

                        ; test for the first digit
    mov sil, byte [rdi+r11]
    inc r11

    cmp sil, '0'
    jb .failed
    cmp sil, '9'
    ja .failed
                        ; so there is a number, we can start looping
    .multiloop:
    cmp sil, '0'
    jb .ok
    cmp sil, '9'
    ja .ok
                        ; that's a digit, time to multiply and append
    mul r10
    sub sil, '0'
    add al, sil
                        ; cool, we read the next character
    mov sil, byte [rdi+r11]
    inc r11
    jmp .multiloop

    .ok:                ; number read successfully
    dec r11
    mov rdx, r11
    ret

    .failed:            ; number not read successfully
    xor rax, rax
    xor rdx, rdx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    push rbx
    xor rbx, rbx

    cmp byte[rdi], '+'
    je .skipsign        ; so it's a positive number, just skip the sign and roll with it
    cmp byte[rdi], '-'
    jne .ready          ; if there's no sign, no need to skip anything

    inc rbx             ; negative signed number, let's remember that

    .skipsign:
    inc rdi
    inc rbx             ; let's remember that's it's signed in general
                        ; we're ready to parse
    .ready:
    call parse_uint
    test rbx, rbx       ; check if it's neither signed or negative
    je .done
    inc rdx             ; add one to the length if it was signed

    cmp rbx, 2          ; if it was negative - invert it after parsing, done
    jne .done
    neg rax

    .done:
    pop rbx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax ; character counter

    .copyloop:

    cmp rdx, rax                   ; compare the character counter to the buffer length, maybe we have overflown
    jb .oops                       ; yep, jb is correct, jae doesn't work
    mov cl, [rdi+rax]              ; copying the character
    mov [rsi+rax], cl              ; pasting the character
    test rcx, rcx                  ; checking if we just pasted a NULL, then we're done
    je .over
    inc rax
    jmp .copyloop

    .oops:
    xor rax, rax
    .over:
    ret
