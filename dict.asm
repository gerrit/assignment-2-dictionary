%include "lib.inc"

global find_word

find_word:
push r12                 ; pair iterator
mov r12, rsi
.findloop:
test r12, r12
je .eek
lea rsi, [r12+8]         ; move the first key character
call string_equals       ; compare the strings
test rax, rax
jne .over
mov r12, [r12]           ; move to the next (technically previous) pair
jmp .findloop

.eek:
xor r12, r12             ; clear r12 so rax receives a 0
.over:
mov rax, r12
pop r12
ret

