import subprocess

inputs = ["pomni", "ragatha", "gangle", "caine", "bubble", "zooble", "kinger", "jax", "[ABSTRACTED]"*50, ""]
outputs = [
  	["she/her", ""],
  	["she/her", ""],
  	["she/her", ""],
  	["he/him", ""],
  	["", "The key has not been found."],
  	["they/them", ""],
  	["he/him", ""],
  	["he/him", ""],
  	["", "The key entered is too long!"],
	["", "The key entered is empty."]
]

def try_me(strin, strout, strerr):
	process = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	testout, testerr = process.communicate(input=strin.encode())
	if testout.decode() == strout and testerr.decode() == strerr:
		return "FF"
	else:
		return "SF, ANSWER [" + testout.decode() + "], <" + testerr.decode() + "> IS UNSATISFACTORY"

print("COMMENCE THE TRIAL")
res = "HAS SUCCEEDED"
for i in range(len(inputs)):
	temp = try_me(inputs[i], outputs[i][0], outputs[i][1])
	if temp != "FF":
		res = "HAS FAILED"
	print("TEST " + str(i+1) + ": " + temp)


print("THE SUBJECT " + res)


