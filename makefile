N=nasm -felf64 -o

%.o: %.asm
        @$(N) $@ $<

dict.o: dict.asm lib.inc
main.o: main.asm lib.inc dict.inc

.PHONY: test
test:
        @python3 test.py
