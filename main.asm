%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define bufsize 255

global _start

section .rodata
        keylong: db "The key entered is too long!", 0
        keynotfound: db "The key has not been found.", 0
        keyempty: db "The key entered is empty.", 0

section .bss
inputbuf: resb bufsize

section .text

_start:
mov rdi, inputbuf
mov rsi, bufsize
call read_word             ; read the word into the buffer
test rax, rax
je .error                  ; if rax is 0 - there's an error
push rdx                   ; save the key length

mov rdi, rax               ; give the buffer to the function
mov rsi, first
call find_word             ; look through the keys
test rax, rax
je .notfound

lea rdi, [rax+8]
pop rax                    ; restore the key length for later use
lea rdi, [rdi+rax+1]       ; adding the key length to the pointer
call print_string
jmp .exit

.error:
test rdx, rdx              ; rdx is set to 0 or 1 in read_word depending on what happened
jne .empty                 ; if it's 1 then the entered was empty, else - buffer overflow

.toolong:
mov rdi, keylong
call print_strerr
jmp .exit

.empty:
mov rdi, keyempty
call print_strerr
jmp .exit

.notfound:
mov rdi, keynotfound
pop rax                    ; restore the stack
call print_strerr

.exit:
call exit




