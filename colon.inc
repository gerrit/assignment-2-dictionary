%define back 0
%macro colon 2
                                              ; added the ifstr and ifid checks
%ifstr %1
        db %1, 0                              ; my cool key
%else
        %error "Argument 1 is not a key!"
%endif

%ifid %2
        %2: dq back                           ; last pair pointer
        %define back %2
%else
        %error "Argument 2 is not an id!"
%endif

%endmacro
